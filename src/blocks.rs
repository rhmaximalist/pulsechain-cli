use chrono::{DateTime, Local};
use std::time::{Duration, UNIX_EPOCH};
use web3::{
    types::{Block, BlockId, BlockNumber, H256, U64},
    Transport, Web3,
};

/// A collection of blocks and desired user fields for display purposes.
pub struct Blocks {
    /// the fetched blocks in descending order
    blocks: Vec<Block<H256>>,
    /// the list of block fields to print
    fields: Vec<String>,
}

/// Fetches details about a single block.
pub async fn fetch_one<T: Transport>(
    web3: Web3<T>,
    block_number: u64,
    fields: Vec<String>,
) -> Blocks {
    let eth = web3.eth();
    let blocks = vec![eth
        .block(BlockId::Number(BlockNumber::Number(U64::from(
            block_number,
        ))))
        .await
        .unwrap()
        .unwrap()];

    return Blocks {
        blocks,
        fields: fields_or_default(fields),
    };
}

/// Fetches details about recent blocks.
pub async fn fetch_recent<T: Transport>(web3: Web3<T>, count: u8, fields: Vec<String>) -> Blocks {
    let eth = web3.eth();
    let latest = eth.block_number().await.unwrap().as_u64();
    let mut blocks = vec![];
    for number in ((latest - count as u64)..latest).rev() {
        blocks.push(
            eth.block(BlockId::Number(BlockNumber::Number(U64::from(number))))
                .await
                .unwrap()
                .unwrap(),
        );
    }

    return Blocks {
        blocks,
        fields: fields_or_default(fields),
    };
}

fn fields_or_default(mut fields: Vec<String>) -> Vec<String> {
    if fields.len() == 0 {
        fields.push("number".to_string());
        fields.push("author".to_string());
        fields.push("hash".to_string());
    } else if fields[0] == "all" {
        fields = all_fields()
    }
    fields
}

fn all_fields() -> Vec<String> {
    vec![
        "number".to_string(),
        "author".to_string(),
        "hash".to_string(),
        "parent_hash".to_string(),
        "uncles_hash".to_string(),
        "state_root".to_string(),
        "transactions_root".to_string(),
        "receipts_root".to_string(),
        "gas_used".to_string(),
        "gas_limit".to_string(),
        "base_fee_per_gas".to_string(),
        "extra_data".to_string(),
        "logs_bloom".to_string(),
        "timestamp".to_string(),
        "difficulty".to_string(),
        "total_difficulty".to_string(),
        "seal_fields".to_string(),
        "uncles".to_string(),
        "transactions".to_string(),
        "size".to_string(),
        "mix_hash".to_string(),
        "nonce".to_string(),
    ]
}

impl std::ops::Index<usize> for Blocks {
    type Output = Block<H256>;

    fn index(&self, index: usize) -> &Self::Output {
        &self.blocks[index]
    }
}

#[allow(unused_must_use)]
impl std::fmt::Display for Blocks {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if self.blocks.len() > 1 {
            writeln!(f, "LATEST BLOCKS");
        } else {
            writeln!(f, "BLOCK");
        }
        for block in &self.blocks {
            for field in &self.fields {
                let field = &field.to_lowercase()[..];
                match field {
                    "hash" => writeln!(f, "  Hash: {:x?}", block.hash.unwrap()),
                    "parent_hash" => writeln!(f, "  Parent Hash: {:x?}", block.parent_hash),
                    "uncles_hash" => writeln!(f, "  Uncles Hash: {:x?}", block.uncles_hash),
                    "author" => writeln!(f, "  Author: {:x?}", block.author),
                    "state_root" => writeln!(f, "  State Root: {:x?}", block.state_root),
                    "transactions_root" => {
                        writeln!(f, "  Transaction Root: {:x?}", block.transactions_root)
                    }
                    "receipts_root" => writeln!(f, "  Receipts Root: {:x?}", block.receipts_root),
                    "number" => writeln!(f, "  Number: {}", block.number.unwrap()),
                    "gas_used" => writeln!(f, "  Gas Used: {}", block.gas_used),
                    "gas_limit" => writeln!(f, "  Gas Limit: {}", block.gas_limit),
                    "base_fee_per_gas" => {
                        writeln!(
                            f,
                            "  Base Fee Per Gas: {}",
                            block.base_fee_per_gas.unwrap_or_default()
                        )
                    }
                    "extra_data" => {
                        let extra_data: String = block
                            .extra_data
                            .0
                            .iter()
                            .map(|byte| format!("{:x}", byte))
                            .collect();
                        writeln!(f, "  Extra Data: 0x{}", extra_data)
                    }
                    "logs_bloom" => writeln!(f, "  Logs Bloom: {:x?}", block.logs_bloom.unwrap()),
                    "timestamp" => {
                        let block_time = DateTime::<Local>::from(
                            UNIX_EPOCH + Duration::from_secs(block.timestamp.as_u64()),
                        );
                        let block_age = Local::now() - block_time;
                        writeln!(
                            f,
                            "  Time: {} (age: {}s)",
                            block_time.format("%Y-%m-%d %r"),
                            block_age.num_seconds()
                        )
                    }
                    "difficulty" => writeln!(f, "  Difficulty: {}", block.difficulty),
                    "total_difficulty" => {
                        writeln!(f, "  Total Difficulty: {}", block.total_difficulty.unwrap())
                    }
                    "seal_fields" => writeln!(f, "  Seal Fields: {:x?}", block.seal_fields),
                    "uncles" => writeln!(f, "  Uncles: {:x?}", block.uncles),
                    "transactions" => writeln!(f, "  Transactions: {:x?}", block.transactions),
                    "size" => writeln!(f, "  Size: {:x?}", block.size.unwrap()),
                    "mix_hash" => writeln!(f, "  Mix Hash: {:x?}", block.mix_hash.unwrap()),
                    "nonce" => writeln!(f, "  Nonce: {}", block.nonce.unwrap()),
                    _ => Ok(()),
                };
            }
            writeln!(f, "");
        }
        Ok(())
    }
}
