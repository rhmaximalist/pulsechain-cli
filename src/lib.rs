pub const DEFAULT_RPC_ENDPOINT: &str = "https://rpc.v2.testnet.pulsechain.com";
pub const BURN_CONTRACT_ADDR: &str = "0x0000000000000000000000000000000000000666";
pub const VALIDATOR_CONTRACT_ADDR: &str = "0x0000000000000000000000000000000000001000";
pub const SLASHING_CONTRACT_ADDR: &str = "0x0000000000000000000000000000000000001001";
pub const STAKING_CONTRACT_ADDR: &str = "0x0000000000000000000000000000000000001002";
pub const MAX_CONCURRENCY: usize = 5;
pub const CURRENCY_SYMBOL: &str = "tPLS";

/// Details about blocks.
pub mod blocks;
/// High-level network status.
pub mod status;
/// Useful utilities.
pub mod utils;
/// Details about network validator nodes.
pub mod validators;
