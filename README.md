# PulseChain CLI

A command-line utility for inspecting a PulseChain network.

## Pre-built Binaries

- Linux: [bin/linux/pcli](bin/linux/pcli)
- Mac: [bin/mac/pcli](bin/mac/pcli)
- Windows: [bin/windows/pcli.exe](bin/windows/pcli.exe)

## Building From Source

```shell
cargo build
```

## Usage Examples

```shell
$> pcli
pulsechain-cli 1.0.0

USAGE:
    pcli [OPTIONS] <SUBCOMMAND>

OPTIONS:
    -e, --endpoint <URL>    Sets the URL to use for RPC calls [default:
                            https://rpc.v2.testnet.pulsechain.com]
    -h, --help              Print help information
    -V, --version           Print version information

SUBCOMMANDS:
    blocks    Commands relating to blocks
    help      Print this message or the help of the given subcommand(s)
    status    Print the general network status
    vals      Commands relating to validators
```

### General Network Status

```shell
$> pcli status
Connecting to blockchain at https://rpc.v2.testnet.pulsechain.com...

CONTRACT BALANCES
  Burn Contract: 5.589657728048394398 tPLS
  Slashing Contract: 0 tPLS
  Staking Contract: 10908078.535509385409337355 tPLS
  Validator Contract: 0.494243783231940654 tPLS

LATEST BLOCK
  Number: 13839812
  Hash: 0x9fb4a2c71be5db5ab11040eaab40d4396d8fc68c9cc9c5a3a24e89aeac48900a
  Time: 2022-01-02 02:16:40 PM (age: 5s)
  Author: 0x9f8ff5790d111915b55ef9d9bbd6e82ef08c4550

CURRENT VALIDATORS
  01: 0x0a3e20d30e1b49532b8f003f4f5bd0e5bed0d1ad | 0 slashes
  02: 0xdb5f55b6111f0adc916921cf5624b2768f6524f8 | 0 slashes
  03: 0x2746826a3d6b57ccd60ec5b5264242379025fa14 | 1 slashes (11535 blocks ago)
  04: 0x7e5df9d1c0d3017ad8891efdedc9309a2c04ebaa | 11 slashes (657 blocks ago)
  05: 0x9865996acd1aa8a02a5129acd07cfdbbfc80789c | 17 slashes (7872 blocks ago)
  06: 0x33c31ee0457adfe7e41fd65a33f50262cc97ea1a | 0 slashes
  07: 0x9f8ff5790d111915b55ef9d9bbd6e82ef08c4550 | 0 slashes
  08: 0xfd5ac22559ffd8f938cc16c966a25fe97598a01b | 0 slashes
  09: 0xb6e7b9e348ff4649db23f0f91ab70648b9057958 | 0 slashes
  10: 0xab322fab3c9ed6d26a90d444bbf234de1e1134e4 | 1 slashes (8221 blocks ago)
  11: 0x39d70a04d7409d3533edd5623c83f7ab3001e949 | 1 slashes (9002 blocks ago)
  12: 0xac90b9879c656f5ddd0ab17739651530e4ecb388 | 0 slashes
  13: 0x878367583d6b7fe9da6775285e5b69d496f04ebb | 0 slashes
  14: 0x13d190d8986df9823eb2f774d11bca90c44c5092 | 0 slashes
  15: 0x9eed930d70e6463fc110549cab7b02503073bbcc | 0 slashes
  16: 0x3adb68e214470bf8da75705679076628982b3d81 | 1 slashes (10541 blocks ago)
  17: 0x4611781a849ec0d094261a3b920ec9be39bd94f2 | 0 slashes
  18: 0x316f05c70504e4c6afcc9904d09785d8368d490a | 0 slashes
  19: 0x423a7093f24fcb427f7a8f9192c5f5d62a78457d | 0 slashes
  20: 0xef56e24ad81c656a74c638720e1d413c88ff6236 | 0 slashes
  21: 0x904ef0e86841bbaf37385f8390221c76e5bca633 | 0 slashes

NEXT ROTATION IN 12988 BLOCKS (est. 07:06:09 AM)
```

### Registered Validators

```shell
$> pcli vals reg
Connecting to blockchain at https://rpc.v2.testnet.pulsechain.com...

REGISTERED VALIDATORS
  01: Address: 0xdb5f55b6111f0adc916921cf5624b2768f6524f8
      Fee Address: 0xdb5f55b6111f0adc916921cf5624b2768f6524f8
      In Rotation: true
      Rev Share: 50%
      Pending Stake: 58.263 tPLS
      Total Stake: 56783.871901327069449454 tPLS

  02: Address: 0x33c31ee0457adfe7e41fd65a33f50262cc97ea1a
      Fee Address: 0x33c31ee0457adfe7e41fd65a33f50262cc97ea1a
      In Rotation: true
      Rev Share: 50%
      Pending Stake: 11.011 tPLS
      Total Stake: 50452.1116149131663546 tPLS

  03: Address: 0x9f8ff5790d111915b55ef9d9bbd6e82ef08c4550
      Fee Address: 0x9f8ff5790d111915b55ef9d9bbd6e82ef08c4550
      In Rotation: true
      Rev Share: 50%
      Pending Stake: 5.506 tPLS
      Total Stake: 50331.5694045273333333 tPLS
  ...
```

### Block Details

```shell
$> pcli blocks list 3 -f number -f author -f difficulty
Connecting to blockchain at https://rpc.v2.testnet.pulsechain.com...

LATEST BLOCKS
  Number: 13839791
  Author: 0x9f8ff5790d111915b55ef9d9bbd6e82ef08c4550
  Difficulty: 2

  Number: 13839790
  Author: 0x9eed930d70e6463fc110549cab7b02503073bbcc
  Difficulty: 2

  Number: 13839789
  Author: 0x9865996acd1aa8a02a5129acd07cfdbbfc80789c
  Difficulty: 2
```

```shell
$> pcli blocks show 1 -f all
Connecting to blockchain at https://rpc.v2.testnet.pulsechain.com...

BLOCK
  Number: 1
  Author: 0x05a56e2d52c817161883f50c441c3228cfe54d9f
  Hash: 0x88e96d4537bea4d9c05d12549907b32561d3bf31f45aae734cdc119f13406cb6
  Parent Hash: 0xd4e56740f876aef8c010b86a40d5f56745a118d0906a34e69aec8c0db1cb8fa3
  Uncles Hash: 0x1dcc4de8dec75d7aab85b567b6ccd41ad312451b948a7413f0a142fd40d49347
  State Root: 0xd67e4d450343046425ae4271474353857ab860dbc0a1dde64b41b5cd3a532bf3
  Transaction Root: 0x56e81f171bcc55a6ff8345e692c0f86e5b48e01b996cadc001622fb5e363b421
  Receipts Root: 0x56e81f171bcc55a6ff8345e692c0f86e5b48e01b996cadc001622fb5e363b421
  Gas Used: 0
  Gas Limit: 5000
  Base Fee Per Gas: 0
  Extra Data: 0x476574682f76312e302e302f6c696e75782f676f312e342e32
  Logs Bloom: 0x00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
  Time: 2015-07-30 10:26:28 AM (age: 202885353s)
  Difficulty: 17171480576
  Total Difficulty: 34351349760
  Seal Fields: []
  Uncles: []
  Transactions: []
  Size: 537
  Mix Hash: 0x969b900de27b6ac6a67742365dd65f55a0526c41fd18e1b16f1a1215c2e66f59
  Nonce: 0x539b…1ec4
```

```shell
$> pcli blocks list -h
pcli-blocks-list 
Print details about recent blocks

USAGE:
    pcli blocks list [OPTIONS] <COUNT>

ARGS:
    <COUNT>    The number of recent blocks to fetch

OPTIONS:
    -f, --field <FIELD>    The block fields to print (defaults: number, author, hash). Can use "all"
                           See https://docs.rs/web3/latest/web3/types/struct.Block.html#fields
    -h, --help             Print help information
```
